#!/bin/bash

#ติดตั้ง mongodb ผ่าน docker-compose

docker-compose up -d

# อ่านไฟล์ .env.frontend และสร้างไฟล์ config.js
echo "const config = {" > ./public/config.js

while read line; do
  if [[ $line =~ ^[A-Z_]+= ]]; then
    key=$(echo $line | cut -d'=' -f1)
    value=$(echo $line | cut -d'=' -f2-)
    echo "  $key: '$value'," >> ./public/config.js
  fi
done < .env.frontend

echo "};" >> ./public/config.js
echo "Object.freeze(config);" >> ./public/config.js

#รันโปรเจกต์
node server.js