const amqp = require('amqplib');

function sendMessage(queue,message,delay_ms)  {
      return new Promise(async (resolve,reject) => {
            try {

                const EXCHANGE = 'message_exchange';
                const connection = await amqp.connect('amqp://guest:guest@localhost');
                const channel = await connection.createChannel();
        
                await channel.assertExchange(EXCHANGE, 'x-delayed-message', {
                    arguments: { 'x-delayed-type': 'direct' }
                });
        
                await channel.assertQueue(queue, { durable: true });
                await channel.bindQueue(queue, EXCHANGE, '');
        
                const delay = delay_ms * 1000; //
        
                channel.publish(EXCHANGE, '', Buffer.from(message), {
                    headers: { 'x-delay': delay }
                });
        
                console.log(`Sent: ${message} with ${delay}ms delay`);
                
                setTimeout(() => {
                    connection.close();
                    resolve();
                }, 500);
            } catch (error) {
                console.error('Error:', error);
                reject(error);
            }
       });
       
}


sendMessage('message_queue',JSON.stringify({userId:'Uc9cf8a4627a9de66f29a235cbc521eaf',messages:[{text:'Hello',type:'text'}]}),10)