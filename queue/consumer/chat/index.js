const { lineApi } = require("../../../utils/line");
const dotenv = require("dotenv");
dotenv.config();
const amqp = require("amqplib");
const RABBIT_URI = process.env.RABBIT_URI;
const QUEUE_NAME = "chat_queue";
const { handleEvent } = require("../../../services/line-dialogflow.service");

async function consumeChatMessages() {
  try {
    const connection = await amqp.connect(RABBIT_URI);
    const channel = await connection.createChannel();

    await channel.assertQueue(QUEUE_NAME, { durable: true });

    console.log(`Waiting for messages in ${QUEUE_NAME}. To exit press CTRL+C`);

    channel.consume(
      QUEUE_NAME,
      (msg) => {
        if (msg !== null) {
          doProcess(msg);
          channel.ack(msg);
        }
      },
      { noAck: false }
    );
  } catch (error) {
    console.error("Error:", error);
  }
}

function doProcess(msg) {
  try {
    const events = JSON.parse(msg.content.toString());
    events.forEach(async (event) => {
        await handleEvent(event);
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

consumeChatMessages();
