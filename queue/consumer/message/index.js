

const { lineApi } = require("../../../utils/line");
const dotenv = require('dotenv');
dotenv.config();
const amqp = require('amqplib');
const RABBIT_URI = `amqp://guest:guest@localhost:5672/`;
const QUEUE_NAME = 'message_queue';

async function consumeMessages() {
    try {
        const connection = await amqp.connect(RABBIT_URI);
        const channel = await connection.createChannel();

        await channel.assertQueue(QUEUE_NAME, { durable: true });

        console.log(`Waiting for messages in ${QUEUE_NAME}. To exit press CTRL+C`);

        channel.consume(QUEUE_NAME, async (msg) => {
            if (msg !== null) {
                console.log(`Received: ${msg.content.toString()}`);
                await doProcess(msg);
                channel.ack(msg);
            }
        }, { noAck: false });
    } catch (error) {
        console.error('Error:', error);
    }
}

async function doProcess(msg){
    try{
        const content = JSON.parse(msg.content.toString())
        console.log(content)
        await lineApi.pushMessage(content.userId, content.messages)
    }catch(error){
        console.error('Error:', error);
    }
}


consumeMessages();
