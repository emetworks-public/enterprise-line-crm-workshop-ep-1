const express = require("express");
const path = require("path");
const config = require("./config");
const cors = require("cors");
const bodyParser = require("body-parser");
const { fileParser } = require("express-multipart-file-parser");
const { appRouter } = require("./routers/app.router");
const mongo = require("./utils/mongodb");
const cron = require("node-cron");
const { cronjobPushMessage } = require("./services/line-message.service");
const { cronjobRichMenu } = require("./services/line-richmenu.service");

const app = express();

const connectMongo = async () => {
  try {
    await mongo.connect();
  } catch (err) {
    throw err;
  }
};

const startServer = () => {
  const port = parseInt(config.PORT || "8080");
  app.use(cors());
  app.use(bodyParser.json({ limit: "50mb" }));
  app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
  app.disable("x-powered-by");
  app.use(
    fileParser({
      rawBodyOptions: {
        limit: "50mb",
      },
    })
  );
  app.use(express.static(path.join(__dirname, "public")));

  app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "public", "index.html"));
  });

    app.get('/product', (req, res) => {
        res.sendFile(path.join(__dirname, 'public', 'product.html'));
    });

    app.get('/chat', (req, res) => {
        res.sendFile(path.join(__dirname, 'public', 'chat.html'));
    });

  app.use("/api", appRouter);
  // cron.schedule("*/1 * * * *", () => {
  //     cronjobPushMessage()
  // });
  // cron.schedule("*/1 * * * *", () => {
  //     cronjobRichMenu()
  // });

  app.listen(port, function () {
    console.log(`App is listening on port ${port}!`);
  });
};


function addJsonToEnv(envVarName, data) {
    process.env[envVarName] = data;
    console.log(`Added ${envVarName} to process.env.`);
  }

async function loadVault() {
  try {
    const vault = require("node-vault")({
      endpoint: "http://127.0.0.1:8200",
    });

    // Initialize Vault
    const loginResult = await vault.userpassLogin({
      username: "admin",
      password: "kP4yAe8Ec9dZALx5",
    });

    const token = loginResult.auth.client_token;
    vault.token = token;

    console.log("Login successful. Token:", token);
    const secretPath = "secret/data/mysecret";

    // await vault.write(secretPath, {
    //     data: {
    //       MONGO_URI: 'mongodb://rootuser:rootpass@localhost:27017/crm?authSource=admin',
    //       CHANNEL_ID: '2005694708',
    //       CHANNEL_SECRET: 'a3ebe33aff377f93c15a3b3b5b4da0c5',
    //       URL_LIFF:'https://liff.line.me/2005694731-vm6QPblQ',
    //       RABBIT_URI: 'amqp://guest:guest@localhost:5672',
    //       OPENAI_API_KEY: 'sk-proj-uFu4DBG0fWqt60mjrlO1T3BlbkFJlDlapz8gy3kdpaESLev8',
    //       PINECONE_API_KEY: 'ceef5ad1-426b-47c0-8e5e-cac3bc707436',
    //       LINE_CHANNEL_SECRET: 'iy8uU2+cTD5dup2oi+/dQJEUJ+CNBAMp3bUJKul3zPUR8tUnfYBcp1Aaz8g4Lrzccd7HsIoubSsl+AYRpWuOZ47AN2wSB/NVvUabQsf5uQNQdMvHKr9TBX0uH8pKP/lGwBvw1gNuCVBqqrOmiboW5gdB04t89/1O/w1cDnyilFU='
    //     }
    // });

    // Read a secret using the authenticated token
   
    const secretResult = await vault.read(secretPath);
    const secretData = secretResult.data.data;

    addJsonToEnv("MONGO_URI", secretData.MONGO_URI);
    addJsonToEnv("CHANNEL_ID", secretData.CHANNEL_ID);
    addJsonToEnv("CHANNEL_SECRET", secretData.CHANNEL_SECRET);
    addJsonToEnv("URL_LIFF", secretData.URL_LIFF);
    addJsonToEnv("RABBIT_URI", secretData.RABBIT_URI);
    addJsonToEnv("OPENAI_API_KEY", secretData.OPENAI_API_KEY);
    addJsonToEnv("PINECONE_API_KEY", secretData.PINECONE_API_KEY);
    
    console.log("Secret read successfully:",secretData.MONGO_URI );
  } catch (err) {
    console.error("Error managing secrets:", err);
  }
}

const start = async () => {
    try {
        //NOTE อย่าลืม run mongo จาก docker ก่อนนะครับ
        await connectMongo();
        startServer();
   
  } catch (err) {
    console.error("Error starting the server", err);
  }
};

start();
