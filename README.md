# Workshop Enterprise LINE CRM Workshop Ep 1

ยินดีต้อนรับสู่ Workshop Enterprise LINE CRM Workshop Ep 1 โปรเจกต์การทำ CRM บน LINE

## ข้อกำหนดเบื้องต้น

- Node.js (เวอร์ชัน 18 ขึ้นไป)
- Docker Compose

# ขั้นตอนการติดตั้งสร้าง Line Messaging API และ Line Login
## สมัครเป็น LINE Developer
1. เข้าไปที่ https://developers.line.biz/console/ แล้วเลือก Log in 
2. เข้าสู่ระบบด้วยบัญชี LINE ของคุณให้เรียบร้อย
3. กรอกชื่อและอีเมล พร้อมกดยอมรับ Agreement จากนั้นกดปุ่ม Create my account เป็นอันเสร็จสิ้นขั้นตอนการสมัครเป็น LINE Developer
## สร้าง Provider
Provider คือ superset ของแอปทั้งหลายที่เราจะพัฒนาขึ้นรวมถึง LIFF app ด้วย โดยการสร้างเพียงให้ระบุชื่อของ Provider ลงไป ซึ่งอาจจะตั้งเป็นชื่อตัวเอง, ชื่อบริษัท, ชื่อทีม หรือชื่อกลุ่มก็ได้

## สร้าง Channel
Channel คือ subset ของ Provider โดยมีอยู่ 3 รูปแบบ คือ LINE Login, Messaging API และ Clova Skill
1. สร้าง Messaging API และ Copy Channel ID , Channel secret เพื่อมา Set ENV
2. สร้าง Line Login และทำการตั้งค่าเปิด Public และ  Link Add Frined กับตัว Messaging ให้เรียบร้อย
   
    2.1 จากนั้นให้ไปที่ TAB Liff และทำการเปิด Toggle shareTargetPicker
    
    2.2 จากนั้นทำการ Create Liff โดยใส่ URL มั่วๆไปก่อนเพื่อที่จะเอา Liff ID ไป Set ที่ ENV แล้ว หลังจากได้ URL จริงๆมาแล้วค่อยกลับมาเปลี่ยน


## ขั้นตอนการติดตั้งและการรันโปรเจกต์

1. Clone repository:
```bash
git clone https://gitlab.com/emetworks-public/enterprise-line-crm-workshop-ep-1.git
 ```
2. เข้าไปในไดเรกทอรีโปรเจกต์:
```bash
cd enterprise-line-crm-workshop-ep-1
```
3. ติดตั้ง dependencies:
```bash
npm install
```
4. ทำการ Config

แก้ไขไฟล์ `.env` และ `.env.frontend`

5. รันโปรเจกต์:
```bash
sh script-run.sh
```
6. รัน ngrok ตาม port ที่เรา RUN ใครไม่มี Download https://ngrok.com/download ตรงนี้เลย

7. จัดเตรียม Rich Menu ผ่าน API ที่เตรียมไว้

จัดเตรียม Rich Menu 4 แบบ Register, Silver, Gold, Diamond เพื่อทำ Dynamic Richmenu 
    
    7.1 หลังจากนั้นไปที่ Folder `richmenu` จะมี Richmenu เตรียมให้ 4 แบบ
    7.2 ทำการ Set Folder `richmenu/Register.png` และนำไปสร้าง https://ex10.tech/ -> Tools -> Rich Menu Editor -> ทำการเพิ่มช่องมา 1 ช่องเพื่อที่จะใส่ URL ที่เป็น Link Register แล้วนำ JSON กลับมายิง API ใน Postman Rich Menu -> Create Richmenu ALL 
    7.3 ทำเหมือนขั้น 7.2 แต่จะทำการเพิ่มช่องเป็น 2 ช่องช่องแรกเป็น URL ที่เป็น Link Register,อีกช่องเป็น Message Action ใส่เป็น สะสมคะแนน แล้วนำ JSON กลับมายิง API ใน Postman Rich Menu -> Create Richmenu Segment โดยจะใส่ keys แต่ละอันแยกกันตามแบบ Richmenu ["diamond"],["gold"],["silver"]

## Postman Collections

[Postman Collections](https://api.postman.com/collections/3909184-60f24bb0-edb6-4aea-bc94-440938849d0b?access_key=PMAT-01HZK4NV8ND23J981QWR8DEHWJ)


## เริ่มต้นใช้งาน LIFF
หลังจากที่คุณมี LIFF app แล้ว คราวนี้เราจะมาลงมือโค้ดกัน โดยการจะใช้งานคำสั่งต่างๆภายใน LIFF เราจะต้องเริ่มจากการ initialize ตัว LIFF app ขึ้นมาก่อน

## Initial LIFF app

1. ไปที่ไฟล์ `public/index.html` ใน Project แล้ว Uncomment ตัว LIFF SDK ใน `<head>`

    ```html
    <script src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>
    ```

2. ไปที่ไฟล์ `public/script.js` บรรทัดที่ 1 แล้ว Uncomment LiffHelper เพื่อ จะเรียกใช้ Function ใน Class ที่เตรียมไว้ในไฟล์ `public/liff.js`

    ```javascript
    const newLiff = new LiffHelper();
    ```

3. หลังจากทำขั้นตอนเหล่านี้แล้วให้ลอง สมัครสมาชิกผ่าน Liff ที่เราไปสร้างไว้ เมื่อสมัครสำเร็จไปที่ไฟล์ `public/script.js` แล้ว Uncomment บรรทัดที่ 3-6 เพื่อที่จะได้รับ UserId จากต้นทาง 
   
    ```javascript
    const params = new URLSearchParams(window.location.search)
   if (params.get('userId')) {
    localStorage.setItem("userRefId", params.get('userId'));
   }
    ```

4. ไปที่ไฟล์ `public/script.js` แล้ว Uncomment บรรทัดที่ 108-113 เพื่อที่จะเปิดใช้งาน Function การแชร์หาเพื่อนในไลน์
    ```javascript
    async function shareLine() {
       let rs = await newLiff.targetPicker(genFlexAffiliate());
       if (rs) {
         alert("แชร์สำเร็จแล้ว");
       }
    }
    ```

5. ไปที่ไฟล์ `public/script.js` แล้ว Uncomment บรรทัดที่ 116-244 เพื่อที่จะเปิดใช้งาน Function เพื่อสร้าง FLEX Messages ที่จะไว้แชร์หาเพื่อน