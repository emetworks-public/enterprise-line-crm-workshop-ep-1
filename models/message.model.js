const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messageSchema = new Schema({
    keys: {
        type: [String]
    },
    userIds: {
        type: [String]
    },
    status: {
        type: String,
        enum: ['pending', 'in_progress', 'completed'],
        default: 'pending'
    },
    type: {
        type: String,
        required: true
    },
    messages: {
        type: [Object],
        required: true
    },
    requestIds: {
        type: [String],
    },
    schedule: {
        type: Date
    },
    result: {
        sentCount: {
            type: Number,
            default: 0
        },
        sentUserIds: {
            type: [String],
            default: []
        },
        totalCount: {
            type: Number,
            required: true
        },
        failedCount: {
            type: Number,
            default: 0
        },
        failedUserIds: {
            type: [String],
            default: []
        }
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
}, {
    timestamps: true
});

const Message = mongoose.model('Message', messageSchema);
module.exports = Message;
