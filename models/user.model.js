const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    userId: { type: String, unique: true },
    displayName: { type: String },
    pictureUrl: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String, unique: true },
    mobile: { type: String, required: true, unique: true },
    gender: { type: String },
    day: { type: Number },
    month: { type: Number },
    year: { type: Number },
    point: { type: Number, default: 0 },
    userRefId: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

const User = mongoose.model('User', userSchema);
module.exports = User;
