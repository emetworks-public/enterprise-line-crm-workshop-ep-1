const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const segmentSchema = new Schema({
    segmentName: { type: String, required: true },
    condition: { type: Object, required: true },
    keyMain: { type: String, required: true },
    key: { type: String, required: true }
}, {
    timestamps: true
});

const Segment = mongoose.model('Segment', segmentSchema);
module.exports = Segment;
