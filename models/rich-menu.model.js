const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const richMenuSchema = new Schema({
    richMenuId: {
        type: String,
        required: true
    },
    richMenu: {
        type: [Object],
        required: true
    },
    status: {
        type: String,
        enum: ['pending', 'in_progress', 'completed'],
        default: 'pending'
    },
    type: {
        type: String,
        enum: ['default', 'ready_to_use'],
        default: 'default'
    },
    userIds: {
        type: [String],
        default: []
    },
    schedule: {
        type: Date,
    },
    keys: {
        type: [String],
        default: []
    }
}, {
    timestamps: true
});

const RichMenu = mongoose.model('RichMenu', richMenuSchema);
module.exports = RichMenu;
