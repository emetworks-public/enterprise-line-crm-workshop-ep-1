const liff = window.liff;

class LiffHelper {
    async init(liffId) {
        await liff.init(
            {
                liffId: liffId,
                withLoginOnExternalBrowser: true
            }
        );
    }

    async getAccessToken(liffId) {
        await this.init(liffId)
        localStorage.setItem("lineToken", await liff.getAccessToken());
    }

    async targetPicker(messages) {
        if (await liff.isApiAvailable('shareTargetPicker')) {
            try {
                return await liff.shareTargetPicker(messages)
            } catch (err) {
                console.log(err)
                throw new Error(err)
            }
        }
    }
    async sendMessages(messages) {
        try {
            const messagesToSend = Array.isArray(messages) ? messages : [messages];
            await liff.sendMessages(messagesToSend)
        } catch (err) {
            alert(err)
        }

    }
    closeWindow() {
        liff.closeWindow();
    }

    async checkFriend() {
        return await liff.getFriendship();
    }

    async getEmail() {
        return await liff.getDecodedIDToken().email;
    }

}