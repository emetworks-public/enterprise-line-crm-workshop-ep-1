const newLiff = new LiffHelper();
//เริ่ม startInit get token และ เช็คว่าเป็นสมาชิกหรือยัง
async function startInit() {

  const params = new URLSearchParams(window.location.search)
  if (params.get('userId')) {
    localStorage.setItem("userRefId", params.get('userId'));
  }
  await newLiff.getAccessToken(config.LIFF_INDEX)
  const check = await newLiff.checkFriend();
  if (!check.friendFlag) {
    window.location.href = config.ADD_FRIEND_URL;
    return;
  }
  const token = localStorage.getItem("lineToken")
  let configAxios = {
    method: 'get',
    maxBodyLength: Infinity,
    url: `${config.BASE_URL_API}/api/user`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  };
  const rs = await axios.request(configAxios)
  if (rs.data) {
    localStorage.setItem("userId", rs.data.userId);
    localStorage.setItem("dataInfo", JSON.stringify(rs.data));
    $("#loader").hide();
    $("#profile").load("profile.html", async function () {
      await startProfile(rs);
    });
  } else {
    $("#loader").hide();
    $("#register").load("register.html")
  }
}



async function initRegister() {
  document.getElementById("email").value = await newLiff.getEmail();
}

//แสดง profile หลังจาก register
async function startProfile(rs) {
  const formData = {
    pictureUrl: rs.data.pictureUrl,
    firstName: rs.data.firstName,
    displayName: rs.data.displayName,
    lastName: rs.data.lastName,
    email: rs.data.email,
    mobile: rs.data.mobile,
    gender: rs.data.gender === "male" ? "ชาย" : "หญิง",
    point: rs.data.tier
  };
  document.getElementById('name').textContent = `${formData.firstName} ${formData.lastName}`;
  document.getElementById('emailS').textContent = `อีเมล : ${formData.email}`;
  document.getElementById('mobileS').textContent = `เบอร์โทร : ${formData.mobile}`;
  document.getElementById('genderS').textContent = `เพศ : ${formData.gender}`;
  document.getElementById('point').textContent = `tier : ${formData.point}`;
  document.getElementById('profileURL').src = formData.pictureUrl;
  document.getElementById('displayName').textContent = formData.displayName;

}
//สมัครสมาชิก
async function submitForm() {
  try {
    document.getElementById("submitButton").disabled = true;
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const email = document.getElementById('email').value;
    const gender = document.querySelector('input[name="gender"]:checked').value;
    const mobile = document.getElementById('mobile').value;
    const userRefId = localStorage.getItem("userRefId")
    const day = document.getElementById('day').value;
    const month = document.getElementById('month').value;
    const year = document.getElementById('year').value;
    const formData = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      mobile: mobile,
      gender: gender,
      day: day,
      month: month,
      year: year,
      ...(userRefId ? { userRefId: userRefId } : {})
    };
    const token = localStorage.getItem("lineToken")
    let configAxios = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${config.BASE_URL_API}/api/user`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      data: formData
    };
    try {
      const rs = await axios.request(configAxios)
      localStorage.setItem("userId", rs.data.userId);
      localStorage.setItem("dataInfo", JSON.stringify(rs.data));
      await newLiff.sendMessages(genFlexRegisterSuccess())
      newLiff.closeWindow()
      $("#register").hide();
      $("#profile").load("profile.html", async function () {
        await startProfile(rs);
      });
    } catch (err) {
      alert("ไม่สำเร็จลองอีกครั้ง")
      document.getElementById("submitButton").disabled = false;
    }
  } catch (err) {
    alert("กรุณากรอก Field ให้ครบถ้วน")
    document.getElementById("submitButton").disabled = false;
  }

}

//แชร์หาเพื่อน
async function shareLine() {
  let rs = await newLiff.targetPicker(genFlexAffiliate());
  if (rs) {
    alert("แชร์สำเร็จแล้ว");
  }
}

//flex สำหรับแชร์หาเพื่อน
function genFlexAffiliate() {
  const urlAffiliate = `${config.BASE_URL_LIFF}?userId=${localStorage.getItem("userId")}`
  const messages = [
    {
      "type": "flex",
      "altText": "เชิญชวนเพื่อนมาสมัครสมาชิก",
      "contents": {
        "type": "bubble",
        "hero": {
          "type": "image",
          "url": "https://business.adobe.com/blog/basics/media_150591153a2d6110ea10eaadda897981f364c5dda.jpeg?width=750&format=jpeg&optimize=medium",
          "size": "full",
          "aspectRatio": "20:13",
          "aspectMode": "cover",
          "action": {
            "type": "uri",
            "uri": urlAffiliate
          }
        },
        "body": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": "เชิญชวนเพื่อนมาสมัครสมาชิก",
              "weight": "bold",
              "size": "xl"
            },
            {
              "type": "text",
              "text": "รับสิทธิประโยชน์มากมายเมื่อเพื่อนของคุณสมัครสมาชิก",
              "size": "sm",
              "color": "#aaaaaa",
              "wrap": true
            },
            {
              "type": "box",
              "layout": "vertical",
              "margin": "lg",
              "spacing": "sm",
              "contents": [
                {
                  "type": "box",
                  "layout": "baseline",
                  "spacing": "sm",
                  "contents": [
                    {
                      "type": "text",
                      "text": "ข้อดี:",
                      "color": "#aaaaaa",
                      "size": "sm",
                      "flex": 1
                    },
                    {
                      "type": "text",
                      "text": "1. รับส่วนลดพิเศษ",
                      "wrap": true,
                      "color": "#666666",
                      "size": "sm",
                      "flex": 4
                    }
                  ]
                },
                {
                  "type": "box",
                  "layout": "baseline",
                  "spacing": "sm",
                  "contents": [
                    {
                      "type": "text",
                      "text": "2. รับข่าวสารและโปรโมชั่นก่อนใคร",
                      "wrap": true,
                      "color": "#666666",
                      "size": "sm",
                      "flex": 4
                    }
                  ]
                },
                {
                  "type": "box",
                  "layout": "baseline",
                  "spacing": "sm",
                  "contents": [
                    {
                      "type": "text",
                      "text": "3. สะสมแต้มและรับของรางวัล",
                      "wrap": true,
                      "color": "#666666",
                      "size": "sm",
                      "flex": 4
                    }
                  ]
                }
              ]
            }
          ]
        },
        "footer": {
          "type": "box",
          "layout": "vertical",
          "spacing": "sm",
          "contents": [
            {
              "type": "button",
              "style": "primary",
              "height": "sm",
              "action": {
                "type": "uri",
                "label": "สมัครสมาชิก",
                "uri": urlAffiliate
              },
              "color": "#1DB446"
            },
            {
              "type": "spacer",
              "size": "sm"
            }
          ],
          "flex": 0
        }
      }
    }

  ];

  return messages

}

//flex สำหรับสมัครสมาชิกสำเร็จ
function genFlexRegisterSuccess() {
  const message = [
    {
      "type": "flex",
      "altText": "สมัครสมาชิกเรียบร้อยแล้ว",
      "contents": {
        "type": "bubble",
        "header": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": "สมัครสมาชิกเรียบร้อยแล้ว",
              "weight": "bold",
              "size": "xl",
              "align": "center"
            }
          ]
        },
        "body": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": "ขอบคุณที่สมัครสมาชิกกับเรา",
              "align": "center",
              "margin": "md"
            },
            {
              "type": "text",
              "text": "คลิกปุ่มด้านล่างเพื่อชมโปรไฟล์ของคุณ",
              "align": "center",
              "margin": "md"
            }
          ]
        },
        "footer": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "button",
              "style": "primary",
              "action": {
                "type": "uri",
                "label": "ชมโปรไฟล์ของคุณ",
                "uri": config.BASE_URL_LIFF
              }
            }
          ]
        }
      }
    }

  ]
  return message
}

async function productInit() {
  // alert(newLiff)
  await newLiff.getAccessToken(config.LIFF_PRODUCT)
  const token = localStorage.getItem("lineToken")
  // alert(token)
}

async function selectOption(option) {

  const token = localStorage.getItem("lineToken")
  if (option === "option1") {
    let configAxios = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${config.BASE_URL_API}/api/user/burn-point`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      data: {
        point: 1
      }
    };
    try {
      const rs = await axios.request(configAxios)
      alert("สำเร็จ")
    } catch (err) {
      alert("ไม่สำเร็จลองอีกครั้ง")
    }
  }

  if (option === "option2") {
    let configAxios = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${config.BASE_URL_API}/api/user/burn-point`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      data: {
        point: 2
      }
    };
    try {
      const rs = await axios.request(configAxios)
      alert("สำเร็จ")
    } catch (err) {
      alert("ไม่สำเร็จลองอีกครั้ง")
    }
  }
  newLiff.closeWindow()
}