const { Router } = require("express");
const Joi = require('joi');
const { validateReq } = require("../utils/common");
const pushMessageService = require("../services/line-message.service");
const richmenuService = require("../services/line-richmenu.service");
const sizeOf = require('image-size');

// ส่ง message แบบ segment หรือ broadcast
async function sendMessages(req, res) {
  try {
    const schema = Joi.object({
      keys: Joi.array().items(Joi.string()).min(1).optional(),
      type: Joi.string().valid("segment", "broadcast").required(),
      schedule: Joi.number().optional(),
      messages: Joi.array().min(1).required(),
    });

    let params = validateReq(req.body, schema);
    await pushMessageService.handlePushMessages(params)
    res.json({});
  } catch (err) {
    res.status(400).json({
      message: err.message
    });
  }
}

// สร้าง Rich Menu สำหรับ ALL Users
async function createRichMenu(req, res) {
  try {
    const schema = Joi.object({
      files: Joi.array().items(
        Joi.object({
          buffer: Joi.binary().max(1 * 1024 * 1024).required()
        }).unknown(true)
      ).min(1).required(),
      richMenu: Joi.string().required(),
      schedule: Joi.string().optional(),
    });
    req.body.files = req.files
    const params = validateReq(req.body, schema);

    const file = params.files[0];
    const size = sizeOf(file.buffer);
    const validSizes = [
      { width: 2500, height: 1686 },
      { width: 2500, height: 843 },
      { width: 1200, height: 810 },
      { width: 1200, height: 405 },
      { width: 800, height: 540 },
      { width: 800, height: 270 },
    ];

    const isValidSize = validSizes.some(dim => dim.width === size.width && dim.height === size.height);

    if (!isValidSize) {
      const validSizeDescriptions = validSizes.map(dim => `${dim.width}x${dim.height}`).join(", ");
      throw new Error(`Image must be one of the following sizes: ${validSizeDescriptions}.`);
    }

    const result = await richmenuService.createRichMenuDefault(params);
    res.json(result);
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
}

//สร้าง Rich Menu ตาม Segments
async function createRichMenuSeg(req, res) {
  try {
    const schema = Joi.object({
      files: Joi.array().items(
        Joi.object({
          buffer: Joi.binary().max(1 * 1024 * 1024).required()
        }).unknown(true)
      ).min(1).required(),
      richMenu: Joi.string().required(),
      keys: Joi.string().optional(),
    });
    req.body.files = req.files
    const params = validateReq(req.body, schema);

    const file = params.files[0];
    const size = sizeOf(file.buffer);
    const validSizes = [
      { width: 2500, height: 1686 },
      { width: 2500, height: 843 },
      { width: 1200, height: 810 },
      { width: 1200, height: 405 },
      { width: 800, height: 540 },
      { width: 800, height: 270 },
    ];

    const isValidSize = validSizes.some(dim => dim.width === size.width && dim.height === size.height);

    if (!isValidSize) {
      const validSizeDescriptions = validSizes.map(dim => `${dim.width}x${dim.height}`).join(", ");
      throw new Error(`Image must be one of the following sizes: ${validSizeDescriptions}.`);
    }
    const result = await richmenuService.createRichMenuSegment(params);
    res.json(result);
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
}


const lineRouter = Router();
lineRouter.post("/send-message", sendMessages);
lineRouter.post("/richmenu/default", createRichMenu);
lineRouter.post("/richmenu/segment", createRichMenuSeg);

module.exports = { lineRouter };
