const { Router } = require("express");
const Joi = require('joi');
const { validateReq } = require("../utils/common");
const userService = require("../services/user.service");
const { lineMiddleware } = require("../middlewares/line.middleware");

// สมัครสมาชิก
async function register(req, res) {
    try {
        const personSchema = Joi.object({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            email: Joi.string().email({ tlds: { allow: false } }).required(),
            mobile: Joi.string().pattern(/^[0-9]{10}$/).required(),
            day: Joi.number().integer().min(1).max(31).required(),
            month: Joi.number().integer().min(1).max(12).required(),
            year: Joi.number().integer().min(1900).max(2100).required(),
            gender: Joi.string().valid("male", "female").required(),
            userRefId: Joi.string().allow("", null).optional(),
        });

        let params = validateReq(req.body, personSchema);
        params["userId"] = req.userId;
        params["displayName"] = req.displayName;
        params["pictureUrl"] = req.pictureUrl;
        const rs = await userService.register(params);
        res.json(rs);
    } catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
}

//get profile
async function getLineInfomation(req, res) {
    try {
        const rs = await userService.getInfomation(req);
        res.json(rs);
    } catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
}
async function burnPoint(req, res) {
    try {
        const rs = await userService.burnPoint(req);
        res.json(rs);
    } catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
}

const userRouter = Router();
userRouter.post("/", lineMiddleware, register);
userRouter.post("/burn-point", lineMiddleware, burnPoint);
userRouter.get("/", lineMiddleware, getLineInfomation);

module.exports = { userRouter };
