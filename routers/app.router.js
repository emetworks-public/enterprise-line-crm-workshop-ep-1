const { Router } = require("express");
const { healthCheckRouter } = require("./healthcheck.router");
const { userRouter } = require("./user.router");
const { lineRouter } = require("./line.router");
const { trainRouter } = require("./train.router");  
const { webhookRouter} = require("./webhook.router");
const { queryRouter} = require("./query.router");

const appRouter = Router();

appRouter.use("/", healthCheckRouter);
appRouter.use("/user", userRouter);
appRouter.use("/line", lineRouter);
appRouter.use("/train", trainRouter)
appRouter.use("/webhook", webhookRouter);
appRouter.use("/query", queryRouter);
module.exports = { appRouter };


