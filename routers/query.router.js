const line = require("@line/bot-sdk");

// Line client
const client = new line.Client({
  channelAccessToken: process.env.LINE_CHANNEL_SECRET,
});

const { Router } = require("express");
const {
  queryVectorDocuments,
  callChatGPT,
} = require("../services/openai.service");
const { loadQAStuffChain } = require("langchain/chains");
const { Document } = require("langchain/document");
const { genThankFlexMessage } = require("../utils/flex");
const { updateUserPoint } = require("../services/user.service");
const { removeUserIds } = require("../repositories/rich-menu.repository");
const LangchainOpenAI = require("@langchain/openai").OpenAI;

require("dotenv").config(); // Load environment variables from .env

const queryRouter = Router();
queryRouter.post("/", handleQuestionQuery);
//NOTE จาก class  
queryRouter.post("/fullfilment", handleFullfilement);
//NOTE จากโจทย์ workshop 
queryRouter.post("/choices", handleChoicesFullfilment);

const llm = new LangchainOpenAI({
  openAIApiKey: process.env.OPENAI_API_KEY,
});
const chain = loadQAStuffChain(llm);

async function handleQuestionQuery(req, res) {
  const { query } = req.body;

  try {
    const concatenatedText = await queryVectorDocuments(query);
    let result;

    if (concatenatedText) {
      result = await chain.call({
        input_documents: [new Document({ pageContent: concatenatedText })],
        question: query,
      });

      const lowerCaseText = result.text.trim().toLowerCase();
      
      //NOTE กรณีบอทไม่สามารถตอบได้ จะทำการส่งไปเรียก ChatGPT
      if (
        lowerCaseText.includes("i don't know") ||
        lowerCaseText.includes("i'm not sure")
      ) {
        result = { text: await callChatGPT(query) };
      }
      res.json({ answer: result.text });
    } else {
      result = { text: await callChatGPT(query) };
      res.json({ answer: result.text });
    }
  } catch (error) {
    console.error("Error processing request:", error.message);
    res
      .status(500)
      .json({ error: "An error occurred while processing your request." });
  }
}

async function handleFullfilement(req, res) {
  const { queryResult } = req.body;
  console.log(req.body.session);
  const userSessionInfo = req.body.session.split("/").pop();
  let userId = null;
  let replyToken = null;
  if (userSessionInfo) {
    userId = userSessionInfo.split("#")[0];
    replyToken = userSessionInfo.split("#")[1];
  }

  if (!queryResult || !userId || !replyToken) return;

  // Vector Query
  const concatenatedText = await queryVectorDocuments(queryResult.queryText);
  const vectorResult = await chain.call({
    input_documents: [new Document({ pageContent: concatenatedText })],
    question: queryResult.queryText,
  });

  console.log(`"\n🤠 Mr.Popo answers" : ${vectorResult?.text}\n`);

  if (vectorResult?.text) {
    replyMessage(replyToken, vectorResult?.text);
  } else {
    replyMessage(replyToken, "ขออภัยครับยังไม่มีข้อมูลส่วนนี้");
  }

  res.sendStatus(200);
}

function replyMessage(replyToken, message) {
  const replyMessage = {
    type: "text",
    text: message,
  };

  client
    .replyMessage(replyToken, replyMessage)
    .then(() => {
      console.log("Message replied");
    })
    .catch((err) => {
      console.error("Error replying message:", err);
    });
}

async function handleChoicesFullfilment(req, res) {
  const { queryResult } = req.body;
  console.log(req.body.session);
  const userSessionInfo = req.body.session.split("/").pop();
  let userId = null;
  let replyToken = null;
  if (userSessionInfo) {
    userId = userSessionInfo.split("#")[0];
    replyToken = userSessionInfo.split("#")[1];
  }

  if (!queryResult || !userId || !replyToken) return;
  
  //NOTE ทำหน้าที่ detect กรณีได้รับข้อความจาก fullfilment และแยกเคสการตอบกลับของ Option1 - Option5
  switch (queryResult.queryText) {
    case "Option1":
    case "Option2":
    case "Option3":
      const imageUrl =
        "https://img.freepik.com/free-photo/3d-illustration-closed-black-gift-box_107791-18203.jpg?size=338&ext=jpg&ga=GA1.1.1141335507.1719446400&semt=sph";
      const imageMessage = {
        type: "image",
        originalContentUrl: imageUrl,
        previewImageUrl: imageUrl,
      };
      handleRepleMessage(replyToken, imageMessage);
      // const rs1 = await updateUserPoint(userId, 50);
      // console.log(rs1)
      break;
    case "Option4":
    case "Option5":
      const flexMessage = genThankFlexMessage();
      handleRepleMessage(replyToken, flexMessage);

      //console.log(rs2);
      break;
  }
  const rs2 = await updateUserPoint(userId, 50);
  console.log(rs2);

  res.sendStatus(200);
}

//NOTE ใช้สำหรับตอบข้อความของ user
function handleRepleMessage(replyToken, lineMessage) {
  client
    .replyMessage(replyToken, lineMessage)
    .then(() => {
      console.log("Message replied");
    })
    .catch((err) => {
      console.error("Error replying message:", err);
    });
}
module.exports = { queryRouter };
