const { Router } = require("express");

async function handleHealthCheck(req, res) {
  res.json({ "api": "1.0.1" });
}

const healthCheckRouter = Router();
healthCheckRouter.get("/", handleHealthCheck);

module.exports = { healthCheckRouter };
