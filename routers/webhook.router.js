const express = require("express");
const crypto = require("crypto");
const { handleEvent } = require("../services/line-dialogflow.service");

const { Router } = require("express");
const { sendMq } = require("../utils/rabbit_queue");


const webhookRouter = Router();

webhookRouter.use(
  "/:channelId",
  express.json({ verify: verifyLineSignature })
);

webhookRouter.post("/:channelId", handleChannelWebhook);

// Middleware to verify Line signature dynamically
async function verifyLineSignature(req, res, buf, encoding) {
  const channelId = req.params.channelId;
  if (!channelId) {
    res.status(400).send("ChannelID is required");
    return;
  }

  const signature = req.headers["x-line-signature"];
  const CHANNEL_SECRET = process.env.LINE_CHANNEL_SECRET;
  const hash = crypto
    .createHmac("sha256", CHANNEL_SECRET)
    .update(buf)
    .digest("base64");
  if (signature !== hash) {
    res.status(403).send("Invalid signature");
    throw new Error("Invalid signature");
  }
}

async function handleChannelWebhook(req, res) {
    
  const events = req.body.events;
  // if(!events) return
  
  // events.forEach((event) => {
  //   handleEvent(event);
  // });

  var json = JSON.stringify(events)
  await sendMq('chat_queue',json);
  res.sendStatus(200);
}


module.exports = { webhookRouter };
