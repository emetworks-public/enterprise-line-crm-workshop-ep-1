const path = require("path");

const { Router } = require("express");
const { embedVectorDocuments } = require("../utils/documentUtils");
const { readDocumentFromCSV } = require("../utils/trainUtils");

const trainRouter = Router();
trainRouter.get("/", handleBotTraining);

const DEFAULT_FILE_PATH = path.join(__dirname, "../documents", "products.csv");
const DEFAULT_FILE_TYPE = "csv";

async function handleBotTraining(req, res) {
  
  const { filePath = DEFAULT_FILE_PATH, fileType = DEFAULT_FILE_TYPE } =
    req.query;

  try {
    if (fileType !== DEFAULT_FILE_TYPE) {
      return res
        .status(400)
        .json({ error: `Unsupported file type. Use "${DEFAULT_FILE_TYPE}".` });
    }

    const docs = await readDocumentFromCSV(filePath);
    await embedVectorDocuments(docs);

    res.json({ message: "Docs embedded successfully" });
  } catch (error) {
    console.error("Error training model:", error.message);
    res
      .status(500)
      .json({ error: "An error occurred while training the model." });
  }
}

module.exports = { trainRouter };






