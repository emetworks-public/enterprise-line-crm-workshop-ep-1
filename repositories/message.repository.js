const Message = require('../models/message.model');

const messageRepository = {
    //create message
    async createMessage(data) {
        try {
            const message = new Message(data);
            return await message.save();
        } catch (error) {
            throw new Error(`Error fetching segments: ${error.message}`);
        }
    },
    //ดึง message ที่ถึงเวลาที่กําหนด
    async getScheduledMessages() {
        try {
            const currentDate = new Date();
            const rs = await Message.find({
                schedule: { $exists: true, $lte: currentDate },
                status: "pending"
            })
            return rs;
        } catch (error) {
            throw new Error(`Error fetching segments: ${error.message}`);
        }
    }
};

module.exports = messageRepository;
