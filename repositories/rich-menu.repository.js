const RichMenu = require('../models/rich-menu.model');

const richMenuRepository = {
    //สร้าง Rich Menu
    async createRichMenu(data) {
        try {
            const richMenu = new RichMenu(data);
            return await richMenu.save();
        } catch (error) {
            throw new Error(`Error fetching richMenu: ${error.message}`);
        }
    },
    // ดึง Rich Menu ที่ถึงเวลาที่กําหนด
    async getScheduledRichmenus() {
        try {
            const currentDate = new Date();
            const rs = await RichMenu.find({
                schedule: { $exists: true, $lte: currentDate },
                status: "pending"
            })
            return rs;
        } catch (error) {
            throw new Error(`Error fetching richMenu: ${error.message}`);
        }
    },
    // ดึง Rich Menu ที่มี keys ที่ตรงกับ key
    async getRichmenuBykeyLastCreated(key) {
        try {
            const rs = await RichMenu.findOne({
                keys: key,

            }).sort({ "createdAt": -1 })
            return rs;
        } catch (error) {
            throw new Error(`Error fetching richMenu: ${error.message}`);
        }
    },

    //ลบ userId ออกจาก Rich Menu
    async removeUserIds(userId) {
        try {
            await RichMenu.updateMany(
                { userIds: userId },
                { $pull: { userIds: userId } }
            );
        } catch (error) {
            throw new Error(`Error fetching richMenu: ${error.message}`);
        }
    }

};

module.exports = richMenuRepository;
