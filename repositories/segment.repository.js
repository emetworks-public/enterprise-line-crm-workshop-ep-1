const Segment = require('../models/segment.model');

const segmentRepository = {
    //get all segments
    async getAllSegments() {
        try {
            return await Segment.find({});
        } catch (error) {
            throw new Error(`Error fetching segments: ${error.message}`);
        }
    },
    //get segments by key
    async getSegmentsByKey(keys) {
        try {
            return await Segment.find({ key: { $in: keys } });
        } catch (error) {
            throw new Error(`Error fetching segments by key: ${error.message}`);
        }
    },
    //get tier by point
    async getTierByPoint(point) {
        try {
            let tier = "silver"
            const tiers = await Segment.find({
                keyMain: "tier"
            });
            tiers.forEach(e=>{
                tier = e.condition.point["$gte"] <= point ? e.key : tier
            })
            return tier;
        } catch (error) {
            throw new Error(`Error fetching segments by key: ${error.message}`);
        }
    }
};

module.exports = segmentRepository;
