const User = require('../models/user.model');

class UserRepository {
    // สร้างสมาชิก
    async createUser(data) {
        const user = new User(data);
        return await user.save();
    }
    // ค้นหาสมาชิกด้วย mobile หรือ userId
    async getUserByMobileOrLineUserId(mobile, userId) {
        if (mobile) {
            const userByMobile = await User.findOne({ mobile: mobile });
            if (userByMobile) {
                return userByMobile;
            }
        }
        return await User.findOne({ userId: userId });
    }

    // ค้นหาสมาชิกด้วย userId
    async getUserByLineUserId(userId) {
        return await User.findOne({ userId: userId });
    }
    async findAll() {
        return await User.find({});
    }

    // ค้นหาสมาชิกทั้งหมดตามเงื่อนไข
    async fetchUserLineExitingAndConditions(conditions) {
        conditions.push({ userId: { $exists: true } });
        console.log(JSON.stringify(conditions))
        const users = await User.find(
            { $and: conditions },
            { userId: 1, _id: 0 } // Projection to include userId and exclude _id
        );
        const userIds = users.map(user => user.userId);
        return userIds;
    }
}

module.exports = new UserRepository();
