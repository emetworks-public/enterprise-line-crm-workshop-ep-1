const { lineApi } = require("../utils/line");

const lineMiddleware = async (req, res, next) => {
    const authHeader = req.headers["authorization"];

    if (!authHeader) {
        return res.status(401).json({ message: "Authorization Required" });
    }

    const [type, token] = authHeader.split(" ");

    if (type !== "Bearer" || !token) {
        return res.status(401).json({ message: "Authorization Required" });
    }

    try {
        await lineApi.verifyToken(token);
        const line = await lineApi.getUserProfile(token);
        req.userId = line.userId;
        req.displayName = line.displayName;
        req.pictureUrl = line.pictureUrl;
        next();
    } catch (err) {
        console.log(err)
        return res.status(401).json({ message: "Authorization Required" });
    }
};

module.exports = { lineMiddleware };
