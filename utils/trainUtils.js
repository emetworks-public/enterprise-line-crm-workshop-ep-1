const fs = require('fs');
const csv = require('csv-parser');

async function readDocumentFromCSV(filePath) {
  return new Promise((resolve, reject) => {
    const schedule = [];
    fs.createReadStream(filePath)
      .pipe(csv())
      .on('data', (row) => {
        schedule.push({
          query: row.query,
          response: row.response,
        });
      })
      .on('end', () => {
        resolve(schedule);
      })
      .on('error', reject);
  });
}

module.exports = {
  readDocumentFromCSV,
};
