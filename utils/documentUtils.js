const fs = require('fs');
const csv = require('csv-parser');
const { v4: uuidv4 } = require('uuid');
const { Pinecone } = require('@pinecone-database/pinecone');
const { OpenAIEmbeddings } = require('@langchain/openai');

const pc = new Pinecone({
  apiKey: process.env.PINECONE_API_KEY,
});

const embeddings = new OpenAIEmbeddings({
  openAIApiKey: process.env.OPENAI_API_KEY,
  batchSize: 100,
  model: "text-embedding-ada-002",
});

const indexName = "products";
const index = pc.index(indexName);

async function embedVectorDocuments(docs) {
  const docEmbeddings = await embeddings.embedDocuments(docs.map(item => item.query));
  const docVectors = docEmbeddings.map((embedding, i) => ({
    id: uuidv4(),
    values: embedding,
    metadata: {
      text: docs[i].query,
      answer: docs[i].response,
    },
  }));

  await index.upsert(docVectors);
}

async function readDocumentFromCSV(filePath) {
  return new Promise((resolve, reject) => {
    const schedule = [];
    fs.createReadStream(filePath)
      .pipe(csv())
      .on('data', (row) => {
        schedule.push({
          query: row.query,
          response: row.response,
        });
      })
      .on('end', () => {
        resolve(schedule);
      })
      .on('error', reject);
  });
}

module.exports = {
  embedVectorDocuments,
  readDocumentFromCSV,
};
