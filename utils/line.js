const axios = require("axios");
const { createUUID } = require("../utils/common");
const config = require("../config");
const baseLineUrl = "https://api.line.me";
const baseLineUrlUpload = "https://api-data.line.me";

class LineApi {
  async getTokenStatelate() {
    try {
      const endpoint = `${baseLineUrl}/oauth2/v3/token`;
      const response = await axios({
        method: "post",
        url: endpoint,
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        data: {
          grant_type: "client_credentials",
          client_id: `${config.CHANNEL_ID}`,
          client_secret: `${config.CHANNEL_SECRET}`,
        },
      });
      return response.data.access_token;
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async verifyToken(accessToken) {
    try {
      const endpoint = `${baseLineUrl}/oauth2/v2.1/verify?access_token=${accessToken}`;
      const response = await axios.get(endpoint);
      return response.data;
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async getUserProfile(accessToken) {
    try {
      const endpoint = `${baseLineUrl}/v2/profile?access_token=${accessToken}`;
      const response = await axios.get(endpoint, {
        headers: { Authorization: `Bearer ${accessToken}` },
      });
      return response.data;
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async pushMessage(lineUserId, flex) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/message/push`;
      let rs = await axios({
        method: "post",
        url: endpoint,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
          "X-Line-Retry-Key": `${createUUID()}`,
        },
        data: {
          to: lineUserId,
          messages: flex,
        },
      });
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async multicastMessage(lineUserIds, flex) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/message/multicast`;
      let rs = await axios({
        method: "post",
        url: endpoint,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
          "X-Line-Retry-Key": `${createUUID()}`,
        },
        data: {
          to: lineUserIds,
          messages: flex,
        },
      });
      return rs.headers["x-line-request-id"];
    } catch (err) {
      console.log(err.message);
      throw new Error(err.message);
    }
  }

  async broadcastMessage(flex) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/message/broadcast`;
      let rs = await axios({
        method: "post",
        url: endpoint,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
          "X-Line-Retry-Key": `${createUUID()}`,
        },
        data: {
          messages: flex,
        },
      });
      return rs.headers["x-line-request-id"];
    } catch (err) {
      console.log(err.message);
      throw new Error(err.message);
    }
  }

  async checkQuota() {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/message/quota/consumption`;
      let rs = await axios({
        method: "get",
        url: endpoint,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
      });
      return rs.data;
    } catch (err) {
      console.log(err.message);
      throw new Error(err.message);
    }
  }

  async linkRichMenu(lineUserId, richMenuId) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/user/${lineUserId}/richmenu/${richMenuId}`;
      await axios({
        method: "post",
        url: endpoint,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      return {};
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async linkRichMenuMulti(lineUserIds, richMenuId) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/richmenu/bulk/link`;
      const rs = await axios({
        method: "post",
        url: endpoint,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
        data: {
          richMenuId: richMenuId,
          userIds: lineUserIds,
        },
      });
      return {};
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async unlinkRichMenu(lineUserId) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/user/${lineUserId}/richmenu`;
      await axios({
        method: "delete",
        url: endpoint,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async deleteRichMenu(richMenuId) {
    try {
      let accessToken = await this.getTokenStatelate();
      await lineClient.deleteRichMenu(richMenuId);
    } catch (err) {
      console.error("Error deleting rich menu:", err);
      throw new Error("Failed to delete rich menu. Try again.");
    }
  }

  async createRichMenu(data) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/richmenu`;
      const rs = await axios({
        method: "post",
        url: endpoint,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
        data: data,
      });
      return rs.data.richMenuId;
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async setRichMenu(richMenuId, buffer) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrlUpload}/v2/bot/richmenu/${richMenuId}/content`;
      const rs = await axios({
        method: "post",
        url: endpoint,
        headers: {
          "Content-Type": "image/jpeg",
          Authorization: `Bearer ${accessToken}`,
        },
        data: buffer,
      });
      return rs.data;
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }

  async defaultRichMenu(richMenuId) {
    try {
      let accessToken = await this.getTokenStatelate();
      const endpoint = `${baseLineUrl}/v2/bot/user/all/richmenu/${richMenuId}`;
      const rs = await axios({
        method: "post",
        url: endpoint,
        headers: {
          "Content-Type": "image/jpeg",
          Authorization: `Bearer ${accessToken}`,
        },
      });
      return rs.data;
    } catch (err) {
      console.log(err);
      throw new Error(err.message);
    }
  }
}

const lineApi = new LineApi();
module.exports = { lineApi };
