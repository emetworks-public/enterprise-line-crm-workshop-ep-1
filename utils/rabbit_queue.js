const amqp = require('amqplib');

function sendMqDelay(queue,message,delay_ms)  {
    return new Promise(async (resolve,reject) => {
          try {
              const exchange = 'message_exchange';
              const connection = await amqp.connect(process.env.RABBIT_URI);
              const channel = await connection.createChannel();
      
              await channel.assertExchange(exchange, 'x-delayed-message', {
                  arguments: { 'x-delayed-type': 'direct' }
              });
      
              await channel.assertQueue(queue, { durable: true });
              await channel.bindQueue(queue, exchange, '');
      
              const delay = delay_ms * 1000; //
      
              channel.publish(exchange, '', Buffer.from(message), {
                  headers: { 'x-delay': delay }
              });
      
              console.log(`Sent: ${message} with ${delay}ms delay`);
              setTimeout(() => {
                  connection.close();
                  resolve();
              }, 500);
          } catch (error) {
              console.error('Error:', error);
              reject(error);
          }
     });
     
}

function sendMq(queue,message)  {
    return new Promise(async (resolve,reject) => {
          try {
             
              const connection = await amqp.connect(process.env.RABBIT_URI);
              const channel = await connection.createChannel();
      
              await channel.assertQueue(queue, { durable: true });
              channel.sendToQueue(queue, Buffer.from(message));

              console.log(`Sent: ${message}`);
              setTimeout(() => {
                  connection.close();
                  resolve();
              }, 500);
          } catch (error) {
              console.error('Error:', error);
              reject(error);
          }
     });
     
}

module.exports = {
    sendMqDelay,
    sendMq
};
