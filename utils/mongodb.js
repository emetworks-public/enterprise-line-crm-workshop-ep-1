const mongoose = require('mongoose');
// const config = require('../config');
// const { MONGO_URI } = config;

// if (!MONGO_URI) {
//     throw "Please Config Mongo DB";
// }

let connection;

mongoose.set('strictQuery', true);

mongoose.connection.on('connected', () => {
    console.log('mongodb connected');
});

mongoose.connection.on('disconnected', () => {
    console.log('mongodb disconnected');
});

mongoose.connection.on('error', (err) => {
    console.log('mongodb error:', err);
});

const connect = async () => {
    console.log('mongodb connecting ⏱️');
    if (!connection) {
        connection = (await mongoose.connect(process.env.MONGO_URI, {})).connection;
    }
    return connection;
};

module.exports = {
    connect,
};
