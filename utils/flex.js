const config = require("../config");
function genFlexPointRegister(point) {
  const messages = [
    {
      type: "flex",
      altText: "คุณสมัครสมาชิกสำเร็จ",
      contents: {
        type: "bubble",
        header: {
          type: "box",
          layout: "vertical",
          contents: [
            {
              type: "text",
              text: "สมัครสมาชิกสำเร็จ!",
              weight: "bold",
              size: "xl",
              align: "center",
              color: "#4CAF50",
            },
          ],
        },
        body: {
          type: "box",
          layout: "vertical",
          contents: [
            {
              type: "text",
              text: "ขอบคุณที่สมัครสมาชิกกับเรา",
              align: "center",
              margin: "md",
            },
            {
              type: "text",
              text: `คุณได้รับคะแนน ${point} คะแนน`,
              weight: "bold",
              size: "lg",
              align: "center",
              margin: "lg",
              color: "#FF6347",
            },
          ],
        },
        footer: {
          type: "box",
          layout: "vertical",
          contents: [
            {
              type: "button",
              action: {
                type: "uri",
                label: "ดูคะแนนของคุณ",
                uri: config.URL_LIFF,
              },
              style: "primary",
              color: "#4CAF50",
              height: "sm",
              gravity: "center",
            },
          ],
        },
      },
    },
  ];
  return messages;
}

function genFlexPointAffiliate(point) {
  const messages = [
    {
      type: "flex",
      altText: "เพื่อนคุณสมัคร Link ผ่านคุณ",
      contents: {
        type: "bubble",
        header: {
          type: "box",
          layout: "vertical",
          contents: [
            {
              type: "text",
              text: "ยินดีด้วย!",
              weight: "bold",
              size: "xl",
              align: "center",
              color: "#4CAF50",
            },
          ],
        },
        body: {
          type: "box",
          layout: "vertical",
          contents: [
            {
              type: "text",
              text: "เพื่อนคุณสมัคร Link ผ่านคุณ",
              align: "center",
              margin: "md",
            },
            {
              type: "text",
              text: `คุณได้รับคะแนน ${point} คะแนน`,
              weight: "bold",
              size: "lg",
              align: "center",
              margin: "lg",
              color: "#FF6347",
            },
          ],
        },
        footer: {
          type: "box",
          layout: "vertical",
          contents: [
            {
              type: "button",
              action: {
                type: "uri",
                label: "ดูคะแนนของคุณ",
                uri: config.URL_LIFF,
              },
              style: "primary",
              color: "#4CAF50",
              height: "sm",
              gravity: "center",
            },
          ],
        },
      },
    },
  ];
  return messages;
}

function genQuickReplies() {
  return {
    type: "text",
    text: "Please choose an option:",
    quickReply: {
      items: [
        {
          type: "action",
          action: {
            type: "postback",
            label: "Option 1",
            data: "action=option1",
            displayText: "Option 1",
          },
        },
        {
          type: "action",
          action: {
            type: "postback",
            label: "Option 2",
            data: "action=option2",
            displayText: "Option 2",
          },
        },
        {
          type: "action",
          action: {
            type: "postback",
            label: "Option 3",
            data: "action=option3",
            displayText: "Option 3",
          },
        },
        {
          type: "action",
          action: {
            type: "postback",
            label: "Option 4",
            data: "action=option4",
            displayText: "Option 4",
          },
        },
        {
          type: "action",
          action: {
            type: "postback",
            label: "Option 5",
            data: "action=option5",
            displayText: "Option 5",
          },
        },
      ],
    },
  };
}

function genThankFlexMessage() {
  return {
    type: "flex",
    altText: "Flex Message",
    contents: {
      type: "bubble",
      header: {
        type: "box",
        layout: "vertical",
        contents: [
          {
            type: "box",
            layout: "horizontal",
            contents: [
              {
                type: "image",
                url: "https://media.istockphoto.com/id/1198424708/vector/free-gift-sign-and-tag-box-banner.jpg?s=612x612&w=0&k=20&c=DGftEJJdMmU82kpw2wFUTfaJMzJktJKYG8ICo-cU7RA=",
                size: "xxl",
                aspectMode: "cover",
                aspectRatio: "3:4",
                gravity: "center",
                flex: 1,
              },
              {
                type: "box",
                layout: "horizontal",
                contents: [
                  {
                    type: "text",
                    text: "Thank you",
                    size: "xs",
                    color: "#ffffff",
                    align: "center",
                    gravity: "center",
                  },
                ],
                backgroundColor: "#EC3D44",
                paddingAll: "2px",
                paddingStart: "4px",
                paddingEnd: "4px",
                flex: 0,
                position: "absolute",
                offsetStart: "18px",
                offsetTop: "18px",
                cornerRadius: "100px",
                width: "80px",
                height: "25px",
              },
            ],
          },
        ],
        paddingAll: "0px",
      },
      body: {
        type: "box",
        layout: "vertical",
        contents: [
          {
            type: "box",
            layout: "vertical",
            contents: [
              {
                type: "box",
                layout: "vertical",
                contents: [],
                spacing: "sm",
              },
              {
                type: "box",
                layout: "vertical",
                contents: [
                  {
                    type: "text",
                    text: "ขอบคุณสำหรับคุณคนพิเศษ ♥️",
                    color: "#0e89ba",
                  },
                ],
                backgroundColor: "#ffffff1A",
                cornerRadius: "10px",
                margin: "md",
                paddingAll: "10px",
                justifyContent: "center",
                alignItems: "center",
              },
            ],
          },
        ],
        paddingAll: "20px",
        backgroundColor: "#32cdcd",
        background: {
          type: "linearGradient",
          angle: "10deg",
          startColor: "#32cdcd",
          endColor: "#32cdcd",
          centerColor: "#ffffff",
          centerPosition: "1%",
        },
      },
    },
  };
}
module.exports = {
  genFlexPointRegister,
  genFlexPointAffiliate,
  genQuickReplies,
  genThankFlexMessage,
};
