const { isNil, isEmpty } = require('lodash');
function validateReq(params, joiSchema) {
    const result = joiSchema.validate(params);
    if (result.error) {
        throw new Error(result.error.message);
    }
    return result.value;
}


function isObjectEmpty(obj) {
    return isNil(obj) || isEmpty(obj);
}

function createUUID() {
    var dt = new Date().getTime();
    var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
        /[xy]/g,
        function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
        }
    );
    return uuid;
}

function chunkArray(array, chunkSize) {
    const results = [];
    for (let i = 0; i < array.length; i += chunkSize) {
        results.push(array.slice(i, i + chunkSize));
    }
    return results;
}


module.exports = { validateReq, isObjectEmpty, createUUID, chunkArray };
