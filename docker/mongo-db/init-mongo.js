db = db.getSiblingDB('crm');
db.segments.insertMany([
  {
    _id: ObjectId('6656fd8ffc8e0e3bbee1517c'),
    segmentName: 'เพศชาย',
    condition: {
      gender: 'male'
    },
    keyMain: 'gender',
    key: 'male'
  },
  {
    _id: ObjectId('6656fd8ffc8e0e3bbee1517d'),
    segmentName: 'เพศหญิง',
    condition: {
      gender: 'female'
    },
    keyMain: 'gender',
    key: 'female'
  },
  {
    _id: ObjectId('6656ffd5fc8e0e3bbee15182'),
    segmentName: 'tier: silver',
    condition: {
      point: {
        $gte: 0,
        $lte: 11
      }
    },
    keyMain: 'tier',
    key: 'silver'
  },
  {
    _id: ObjectId('6656ffd5fc8e0e3bbee15183'),
    segmentName: 'tier: gold',
    condition: {
      point: {
        $gte: 12,
        $lte: 14
      }
    },
    keyMain: 'tier',
    key: 'gold'
  },
  {
    _id: ObjectId('6656ffd5fc8e0e3bbee15184'),
    segmentName: 'tier: diamond',
    condition: {
      point: {
        $gte: 15
      }
    },
    keyMain: 'tier',
    key: 'diamond'
  },
  {
    _id: ObjectId('66584310fc8e0e3bbee15188'),
    segmentName: 'อายุ 0-30',
    condition: {
      year: {
        $gte: 1994,
        $lte: 2024
      }
    },
    keyMain: 'age',
    key: '0-30'
  },
  {
    _id: ObjectId('66584310fc8e0e3bbee15189'),
    segmentName: 'อายุ 31-60',
    condition: {
      year: {
        $gte: 1964,
        $lte: 1993
      }
    },
    keyMain: 'age',
    key: '31-60'
  },
  {
    _id: ObjectId('66584310fc8e0e3bbee1518a'),
    segmentName: 'อายุ 61-100',
    condition: {
      year: {
        $gte: 1924,
        $lte: 1963
      }
    },
    keyMain: 'age',
    key: '61-100'
  },
  {
    _id: ObjectId('6659a293fc8e0e3bbee15191'),
    segmentName: 'สมาชิก',
    condition: {
      userId: {
        $exists: true
      }
    },
    keyMain: 'main',
    key: 'member'
  }
]);
