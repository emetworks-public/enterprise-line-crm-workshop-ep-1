const userRepository = require("../repositories/user.repository");
const segmentRepository = require("../repositories/segment.repository");
const { isObjectEmpty } = require("../utils/common");
const { lineApi } = require("../utils/line");
const {
  genFlexPointRegister,
  genFlexPointAffiliate,
} = require("../utils/flex");
const lineRichMenuService = require("./line-richmenu.service");
const { sendMqDelay } = require("../utils/rabbit_queue");

//สมัครสมาชิก
async function register(data) {
  try {
    let user = await userRepository.getUserByMobileOrLineUserId(
      data.mobile,
      data.userId
    );

    if (!user) {
      return await handleNewUser(data);
    } else {
      return await updateUser(user, data);
    }
  } catch (err) {
    throw new Error(`Registration error: ${err.message}`);
  }
}

//สร้างสมาชิกใหม่
async function handleNewUser(data) {
  if (data.userRefId && data.userRefId !== data.userId) {
    await updateUserRef(data.userRefId, 2);
  } else {
    data.userRefId = undefined;
  }
  data.point = 10;
  const user = await userRepository.createUser(data);
  await lineApi.pushMessage(data.userId, genFlexPointRegister(10))
  // sendMqDelay(
  //   "message_queue",
  //   JSON.stringify({ userId: data.userId, messages: genFlexPointRegister(10) }),
  //   1000
  // );
  const rs = await genTier(user);
  try {
    await lineRichMenuService.linkRichMenuUserIdAndKey(data.userId, rs.tier);
  } catch (err) {
    console.log(err);
  }
  return rs;
}

//update คะแนน ให้ user ที่แชร์ลิ้งไปให้เพื่อน
async function updateUserRef(userRefId, points) {
  let userRef = await userRepository.getUserByLineUserId(userRefId);
  userRef.point += points;
  await lineApi.pushMessage(userRefId, genFlexPointAffiliate(2));
  // sendMqDelay(
  //   "message_queue",
  //   JSON.stringify({ userId: userRefId, messages: genFlexPointAffiliate(2) }),
  //   1000
  // );
  await userRef.save();
  const rs = await genTier(userRef);
  try {
    await lineRichMenuService.linkRichMenuUserIdAndKey(userRef.userId, rs.tier);
  } catch (err) {
    console.log(err);
  }
}

// อัปเดตข้อมูลสมาชิกที่มีอยู่แล้ว
async function updateUser(user, data) {
  if (!user.userId) {
    user.point += 10;
    await lineApi.pushMessage(data.userId, genFlexPointRegister(10));
    // sendMqDelay(
    //   "message_queue",
    //   JSON.stringify({ userId: data.userId, messages: genFlexPointRegister(10) }),
    //   1000
    // );
  }
  if (
    !user.userId &&
    data.userRefId &&
    data.userRefId !== data.userId &&
    !user.userRefId
  ) {
    await updateUserRef(data.userRefId, 2);
    user.userRefId = data.userRefId;
  }

  Object.assign(user, {
    userId: data.userId,
    displayName: data.displayName,
    pictureUrl: data.pictureUrl,
    firstName: data.firstName,
    lastName: data.lastName,
    day: data.day,
    month: data.month,
    year: data.year,
    email: data.email,
    gender: data.gender,
  });

  await user.save();
  const rs = await genTier(user);
  try {
    await lineRichMenuService.linkRichMenuUserIdAndKey(data.userId, rs.tier);
  } catch (err) {
    console.log(err);
  }
  return rs;
}

//get profile
async function getInfomation(req) {
  try {
    const user = await userRepository.getUserByLineUserId(req.userId);
    if (!isObjectEmpty(user)) {
      user.pictureUrl = req.pictureUrl
      user.displayName = req.displayName
      await user.save()
    }


    return await genTier(user);
  } catch (err) {
    throw new Error(err.message);
  }
}

async function burnPoint(req) {
  try {
    const user = await userRepository.getUserByLineUserId(req.userId);
    user.point -= req.body.point
    await user.save()
    await lineApi.pushMessage(req.userId, [{
      "type": "flex",
      "altText": "แลกของรางวัลสำเร็จ",
      "contents": {
        "type": "bubble",
        "hero": {
          "type": "image",
          "url": "https://example.com/your_image.jpg",
          "size": "full",
          "aspectRatio": "20:13",
          "aspectMode": "cover",
          "action": {
            "type": "uri",
            "uri": "http://example.com"
          }
        },
        "body": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": "แลกของรางวัลสำเร็จ!",
              "weight": "bold",
              "size": "xl",
              "align": "center",
              "margin": "md"
            },
            {
              "type": "text",
              "text": "คุณได้แลกรางวัลเรียบร้อยแล้ว",
              "size": "sm",
              "color": "#666666",
              "align": "center",
              "wrap": true,
              "margin": "md"
            },
            {
              "type": "box",
              "layout": "vertical",
              "margin": "lg",
              "spacing": "sm",
              "contents": [
                {
                  "type": "box",
                  "layout": "baseline",
                  "spacing": "sm",
                  "contents": [
                    {
                      "type": "text",
                      "text": "รางวัล:",
                      "color": "#aaaaaa",
                      "size": "sm",
                      "flex": 2
                    },
                    {
                      "type": "text",
                      "text": "ชื่อรางวัลที่คุณแลก",
                      "wrap": true,
                      "color": "#666666",
                      "size": "sm",
                      "flex": 5
                    }
                  ]
                }
              ]
            }
          ]
        },
        "footer": {
          "type": "box",
          "layout": "vertical",
          "spacing": "sm",
          "contents": [
            {
              "type": "button",
              "style": "link",
              "height": "sm",
              "action": {
                "type": "uri",
                "label": "ดูรายละเอียดเพิ่มเติม",
                "uri": "http://example.com"
              }
            }
          ],
          "flex": 0
        }
      }
    }
    ])

    return {};
  } catch (err) {
    throw new Error(err.message);
  }
}

async function genTier(user) {
  if (!isObjectEmpty(user)) {
    const tier = await segmentRepository.getTierByPoint(user.point);
    const data = {
      ...(user ? user.toObject() : null),
      tier: tier,
    };
    return data;
  }
  return null;
}



module.exports = {
  getInfomation,
  register,
  burnPoint
};
