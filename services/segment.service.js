const userRepository = require("../repositories/user.repository");
const segmentRepository = require("../repositories/segment.repository");

//สร้าง Rules จาก key เพื่อใช้ในการ Query
async function genConditionsByKeys(keys) {
    const segments = await segmentRepository.getSegmentsByKey(keys)

    const groupedConditions = segments.reduce((acc, segment) => {
        const { keyMain, condition } = segment;
        if (!acc[keyMain]) {
            acc[keyMain] = [];
        }
        acc[keyMain].push(condition);
        return acc;
    }, {});

    const andConditions = Object.keys(groupedConditions).map(key => {
        return {
            $or: groupedConditions[key]
        };
    });
    return andConditions

}

//get users จาก conditions ที่ได้จาก Rules ข้างบน
async function getUserLineExitingAndConditions(conditions) {
    try {
        return await userRepository.fetchUserLineExitingAndConditions(conditions)
    } catch (err) {
        throw new Error(err.message);
    }

}

module.exports = {
    genConditionsByKeys,
    getUserLineExitingAndConditions
};