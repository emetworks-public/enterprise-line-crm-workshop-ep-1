const line = require("@line/bot-sdk");
const dialogflow = require("@google-cloud/dialogflow");
const uuid = require("uuid");
const path = require("path");
require("dotenv").config(); // Load environment variables from .env

const { genQuickReplies} = require('../utils/flex');
// Line client
const client = new line.Client({
  channelAccessToken: process.env.LINE_CHANNEL_SECRET,
});

// Dialogflow client
const projectId = "workshop-line-crm-y24-bsbn"; // Replace with your Dialogflow project ID
const keyFilePath = path.join(
  __dirname,
  "../documents/dialogflow/workshop-line-crm-y24-bsbn.json"
);
const sessionClient = new dialogflow.SessionsClient({
  keyFilename: keyFilePath,
});

async function handleEvent(event) {
  switch (event.type) {
    case "message":
      await handleMessageEvent(event);
      break;
    case "follow":
      handleFollowEvent(event);
      break;
    case "unfollow":
      handleUnfollowEvent(event);
      break;
    case "join":
      handleJoinEvent(event);
      break;
    case "leave":
      handleLeaveEvent(event);
      break;
    case "postback":
      handlePostbackEvent(event);
      break;
    case "beacon":
      handleBeaconEvent(event);
      break;
    default:
      console.log(`Unknown event: ${event.type}`);
  }
}

async function handleMessageEvent(event) {
  switch (event.message.type) {
    case "text":
      await handleTextMessage(event);
      break;
    case "image":
      handleImageMessage(event);
      break;
    case "video":
      handleVideoMessage(event);
      break;
    case "audio":
      handleAudioMessage(event);
      break;
    case "location":
      handleLocationMessage(event);
      break;
    case "sticker":
      handleStickerMessage(event);
      break;
    default:
      console.log(`Unknown message type: ${event.message.type}`);
  }
}
function processPayloadMessage(payload) {
  const altText = payload.fields.line.structValue.fields.altText.stringValue;
  const flexContent = generateFlexMessage(
    payload.fields.line.structValue.fields.contents.structValue
  );
  return { altText, contents: flexContent };
}
async function handleTextMessage(event) {
  const replyToken = event.replyToken;
  const userMessage = event.message.text;

  if(userMessage === 'ควิก') {
    handleQuickReplies(replyToken)
  }

  //NOTE #1 bot mimic chat
  // replyMessage(replyToken, userMessage);


  //NOTE #2 bot need dialogflow answer


  //NOTE ทำการเก็บข้อมูล userId กับ replyToken ในตัวแปร sessionId
  let sessionId = event.source.userId+"#"+event.replyToken

  //simple solution for use follow up intent
  if(userMessage === 'แจ้งปัญหา' || userMessage === 'ติดต่อแอดมิน') {
    sessionId = event.source.userId
  }
  
  const sessionPath = sessionClient.projectAgentSessionPath(
    projectId,
    sessionId
  );

  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        text: userMessage,
        languageCode: "th", // Set the language code
      },
    },
  };

  try {
    const response = await sessionClient.detectIntent(request);
  
    await handleDialogflowResponse(response, replyToken);
  } catch (err) {
    console.error("ERROR:", err);
    replyMessage(
      replyToken,
      "There was an error processing your message. Please try again."
    );
  }
}
async function handleQuickReplies(replyToken) {
  const quickReply  = genQuickReplies();

  

  client
    .replyMessage(replyToken, quickReply)
    .then(() => {
      console.log("Message replied");
    })
    .catch((err) => {
      console.error("Error replying message:", err);
    });
}
  
    

async function handleDialogflowResponse(response, replyToken) {
  const result = response[0].queryResult;

  // Check if there is a custom payload for Flex message
  const bubbles = result.fulfillmentMessages
    ?.filter(message => message.message === "payload" || (message.message === "text" && message.text.text[0].trim() !== ''))
    .map(message => {
      if (message.message === "payload") {
        const { altText, contents } = processPayloadMessage(message.payload);
        return {
          type: "flex",
          altText,
          contents,
        };
      } else {
        return {
          type: "text",
          text: message.text.text[0],
        };
      }
    });

  if (bubbles?.length > 0) {
    replyMessages(replyToken, bubbles.slice(0, 5));
  }
}

function handleImageMessage(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, "Thanks for the image!");
}

function handleVideoMessage(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, "Thanks for the video!");
}

function handleAudioMessage(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, "Thanks for the audio!");
}

function handleLocationMessage(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, `Thanks for the location: ${event.message.address}`);
}

function handleStickerMessage(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, "Thanks for the sticker!");
}

function handleFollowEvent(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, "Thank you for following!");
}

function handleUnfollowEvent(event) {
  console.log(`Unfollowed by user: ${event.source.userId}`);
}

function handleJoinEvent(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, "Thanks for inviting me!");
}
 
function handleLeaveEvent(event) {
  console.log(`Left the chat: ${event.source.groupId || event.source.roomId}`);
}

async function handlePostbackEvent(event) {
  console.log(`Postback received: ${event.postback.data}`);
  const replyToken = event.replyToken;
  const postbackData = event.postback.data;

   // Create a unique session ID
   const sessionId = event.source.userId;
   const sessionPath = sessionClient.projectAgentSessionPath(
     projectId,
     sessionId
   );

   
 
   const request = {
     session: sessionPath,
     queryInput: {
       text: {
         text: postbackData,
         languageCode: "th", // Set the language code
       },
     },
   };
 
   try {
     const response = await sessionClient.detectIntent(request);
     await handleDialogflowResponse(response, replyToken);
   } catch (err) {
     console.error("ERROR:", err);
     replyMessage(
       replyToken,
       "There was an error processing your message. Please try again."
     );
   }

  // replyMessage(replyToken, `Received postback: ${event.postback.data}`);
}

function handleBeaconEvent(event) {
  const replyToken = event.replyToken;
  replyMessage(replyToken, "Received beacon event!");
}
//SECTION reply text
function replyMessage(replyToken, message) {
  const replyMessage = {
    type: "text",
    text: message,
  };

  client
    .replyMessage(replyToken, replyMessage)
    .then(() => {
      console.log("Message replied");
    })
    .catch((err) => {
      console.error("Error replying message:", err);
    });
}
//SECTION reply flex message
function replyFlexMessage(replyToken, altText, flexMessage) {
  const replyMessage = {
    type: "flex",
    altText: altText,
    contents: flexMessage,
  };

  client
    .replyMessage(replyToken, replyMessage)
    .then(() => {
      console.log("Flex message replied");
    })
    .catch((err) => {
      console.error("Error replying flex message:", err);
    });
}
//SECTION reply multiple messages
function replyMessages(replyToken, messages) {
  client
    .replyMessage(replyToken, messages)
    .then(() => {
      console.log("message replied");
    })
    .catch((err) => {
      console.log("Error replying message:", err);
    });
}

function generateFlexMessage(contentsStruct) {
  const contents = {};

  // Add properties to contents based on the structValue fields
  Object.keys(contentsStruct.fields).forEach((key) => {
    const field = contentsStruct.fields[key];
    contents[key] = convertField(field);
  });

  return contents;
}

function convertField(field) {
  // Convert individual field based on its type
  switch (field.kind) {
    case "structValue":
      return generateFlexMessage(field.structValue);
    case "listValue":
      return field.listValue.values.map(convertField);
    case "stringValue":
      return field.stringValue;
    case "numberValue":
      return field.numberValue;
    case "boolValue":
      return field.boolValue;
    default:
      return null;
  }
}

module.exports = {
  handleEvent,
};
