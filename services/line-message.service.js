const messageRepository = require("../repositories/message.repository");
const { genConditionsByKeys, getUserLineExitingAndConditions } = require("./segment.service");
const { isObjectEmpty, chunkArray } = require("../utils/common");
const { lineApi } = require("../utils/line");

//สร้าง message ใหม่ ไว้สำหรับยิง segment หรือ broadcast
async function handlePushMessages(data) {
    try {
        switch (data.type) {
            case 'segment':
                await handlePushSegment(data)
                break;
            case 'broadcast':
                await handlePushBroadcast(data);
                break;
            default:
                console.error(`Unknown type: ${type}`);
        }
        return {}
    } catch (err) {
        throw new Error(`error: ${err.message}`);
    }
}

//จัดการ message ที่จะส่งตาม segment อาจจะยิงเลยหรือตั้งเวลาก่อน
async function handlePushSegment(data) {
    try {
        const conditions = await genConditionsByKeys(data.keys)
        const userIds = await getUserLineExitingAndConditions(conditions)
        if (!isObjectEmpty(userIds)) {
            const message = await messageRepository.createMessage({
                keys: data.keys,
                userIds: userIds,
                type: data.type,
                messages: data.messages,
                requestIds: [],
                schedule: data.schedule ? new Date(data.schedule * 1000) : null,
                status: data.schedule ? "pending" : "in_progress",
                result: {
                    sentCount: 0,
                    sentUserIds: [],
                    totalCount: userIds.length,
                    failedCount: 0,
                    failedUserIds: []
                }


            })
            if (!data.schedule) {
                await processPushSegment(message)

            }
            return {}
        }


        throw new Error("Not Found Users In Segments");

    } catch (err) {
        throw new Error(`error: ${err.message}`);
    }

}

//จัดการ message ที่จะส่งทั้งหมด อาจจะยิงเลยหรือตั้งเวลาก่อน
async function handlePushBroadcast(data) {
    const message = await messageRepository.createMessage({
        type: data.type,
        messages: data.messages,
        status: data.schedule ? "pending" : "in_progress",
        requestIds: [],
        schedule: data.schedule ? new Date(data.schedule * 1000) : null,
        result: {
            sentCount: 0,
            sentUserIds: [],
            totalCount: 0,
            failedCount: 0,
            failedUserIds: []
        }
    })
    if (!data.schedule) {
        await processBroadcast(message)
    }
    return {}
}

//ตรวจสอบ message ที่ต้องการส่งว่าถึงเวลาส่งหรือยังถึงก็จะทำการส่งให้เลย
async function cronjobPushMessage() {
    console.log("Start Cronjob Messages")
    try {
        const messages = await messageRepository.getScheduledMessages()
        for (const message of messages) {
            switch (message.type) {
                case 'segment':
                    await processPushSegment(message)
                    break;
                case 'broadcast':
                    await processBroadcast(message);
                    break;
                default:
                    console.error(`Unknown type: ${type}`);
            }
        }

        return {}
    } catch (err) {
        throw new Error(`error: ${err.message}`);
    }

}

//ส่ง message ทั้งหมด
async function processBroadcast(message) {
    message.status = "in_progress"
    await message.save()
    let quotaBefore = await lineApi.checkQuota()
    const rs = await lineApi.broadcastMessage(message.messages)
    let quotaAfter = await lineApi.checkQuota()
    const totalBroadcast = (quotaAfter.totalUsage ? quotaAfter.totalUsage : 0) - (quotaBefore.totalUsage ? quotaBefore.totalUsage : 0)
    message.result.sentCount = totalBroadcast
    message.result.totalCount = totalBroadcast
    message.status = "completed"
    message.requestIds.push(rs)
    await message.save()
}

//ส่ง message ตาม segment
async function processPushSegment(message) {
    message.status = "in_progress"
    const chunks = chunkArray(message.userIds, 500);
    for (const chunk of chunks) {
        try {
            const rs = await lineApi.multicastMessage(chunk, message.messages)
            message.requestIds.push(rs)
            message.result.sentCount += chunk.length
            message.result.sentUserIds.push(...chunk);
        } catch (err) {
            message.result.failedCount += chunk.length
            message.result.failedUserIds.push(...chunk);
        }
        await message.save();
    }
    message.status = "completed"
    await message.save();
}


module.exports = {
    handlePushMessages,
    cronjobPushMessage
};
