const richMenuRepository = require("../repositories/rich-menu.repository");
const { lineApi } = require("../utils/line");
const { genConditionsByKeys, getUserLineExitingAndConditions } = require("./segment.service");

const { isObjectEmpty, chunkArray } = require("../utils/common");

// สร้าง Rich Menu ให้ user ทุกคนเห็น
async function createRichMenuDefault(params) {
    try {
        const richMenu = JSON.parse(params.richMenu)
        const richMenuId = await lineApi.createRichMenu(richMenu)
        await lineApi.setRichMenu(richMenuId, params.files[0].buffer)
        const rs = await richMenuRepository.createRichMenu({
            richMenuId: richMenuId,
            richMenu: richMenu,
            status: params.schedule ? "pending" : "in_progress",
            schedule: params.schedule ? new Date(parseInt(params.schedule) * 1000) : null
        })
        if (!params.schedule) {
            await lineApi.defaultRichMenu(richMenuId)
            rs.status = "completed"
            await rs.save()
        }
        return {}
    } catch (err) {
        throw new Error(`error: ${err.message}`);
    }
}

// ตั้งเวลาให้ user ทุกคนเห็น
async function cronjobRichMenu() {
    console.log("Start Cronjob RichMenu")
    try {
        const richMenus = await richMenuRepository.getScheduledRichmenus()
        for (const richMenu of richMenus) {
            richMenu.status = "in_progress"
            await richMenu.save()
            await lineApi.defaultRichMenu(richMenu.richMenuId)
            richMenu.status = "completed"
            await richMenu.save()
        }
        return {}
    } catch (err) {
        throw new Error(`error: ${err.message}`);
    }

}

// สร้าง Rich Menu ให้ user เฉพาะกลุ่มเห็น
async function createRichMenuSegment(params) {
    try {
        const richMenu = JSON.parse(params.richMenu)
        const keys = JSON.parse(params.keys)
        const conditions = await genConditionsByKeys(keys)
        const userIds = await getUserLineExitingAndConditions(conditions)
        const richMenuId = await lineApi.createRichMenu(richMenu)
        await lineApi.setRichMenu(richMenuId, params.files[0].buffer)
        const rs = await richMenuRepository.createRichMenu({
            richMenuId: richMenuId,
            richMenu: richMenu,
            status: "in_progress",
            userIds: userIds,
            keys: keys
        })
        if (!isObjectEmpty(userIds)) {
            const chunks = chunkArray(userIds, 500);
            for (const chunk of chunks) {
                try {
                    await lineApi.linkRichMenuMulti(chunk, richMenuId)
                } catch (err) {
                    console.log(err)
                }
            }
        }

        rs.status = "completed"
        await rs.save()
        return {}
    } catch (err) {
        throw new Error(`error: ${err.message}`);
    }
}

// ลิงค์ Rich Menu กับ user เฉพาะคน
async function linkRichMenuUserIdAndKey(userId, key) {
    try {
        await richMenuRepository.removeUserIds(userId)
        const data = await richMenuRepository.getRichmenuBykeyLastCreated(key)
        if (!data.userIds.includes(userId)) {
            await lineApi.linkRichMenu(userId, data.richMenuId)
            data.userIds.push(userId);
            await data.save()
        }

        return {}
    } catch (err) {
        throw new Error(`error: ${err.message}`);
    }
}



module.exports = {
    createRichMenuDefault,
    cronjobRichMenu,
    createRichMenuSegment,
    linkRichMenuUserIdAndKey
};
