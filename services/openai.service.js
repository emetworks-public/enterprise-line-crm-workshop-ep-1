const { OpenAIEmbeddings } = require("@langchain/openai");
const OpenAI = require("openai");
const { Pinecone } = require("@pinecone-database/pinecone");

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
  dangerouslyAllowBrowser: true,
  temperature: 0.9,
});

const pc = new Pinecone({
  apiKey: process.env.PINECONE_API_KEY,
});



const indexName = "products";
const index = pc.index(indexName);

async function queryVectorDocuments(query) {
  const queryEmbedding = await new OpenAIEmbeddings().embedQuery(query);
  let queryResponse = await index.query({
    vector: queryEmbedding,
    topK: 3,
    includeMetadata: true,
  });

  const concatenatedText = queryResponse.matches
    .map((match) => `${match.metadata.text}: ${match.metadata.answer}`)
    .join(" ");

  return concatenatedText;
}

async function callChatGPT(prompt) {
  const response = await openai.chat.completions.create({
    model: "gpt-4",
    messages: [{ role: "user", content: prompt }],
  });
  return response.choices[0].message.content;
}

module.exports = {
  queryVectorDocuments,
  callChatGPT,
};
