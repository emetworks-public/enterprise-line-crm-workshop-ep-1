//NOTE use memory conversation

const { OpenAI, OpenAIEmbeddings } = require("@langchain/openai");
const { VectorStoreRetrieverMemory } = require("langchain/memory");
const { LLMChain } = require("langchain/chains");
const { MemoryVectorStore } = require("langchain/vectorstores/memory");
const { PromptTemplate } = require("@langchain/core/prompts");
require("dotenv").config();

const vectorStore = new MemoryVectorStore(new OpenAIEmbeddings({
  apiKey: process.env.OPENAI_API_KEY,
  model: "text-embedding-ada-002",
}));

const memory = new VectorStoreRetrieverMemory({
  vectorStoreRetriever: vectorStore.asRetriever(1),
  memoryKey: "history",
});

// Function to save location-related context to memory


// Function to save general context to memory
async function saveToMemory() {
  await memory.saveContext(
    { input: "My fist love is at 14 years old" },
    { output: "That's good to know." }
  );
  await memory.saveContext(
    { input: "I love listening to rock music" },
    { output: "good taste" }
  );
  await memory.saveContext({ input: "My name is Sake" }, { output: "OK" });
}

// Function to retrieve memory variables
async function retrieveFromMemory(prompt) {
  const memoryVariables = await memory.loadMemoryVariables({ prompt });
  console.log(memoryVariables);
}

// Function to run the conversation chain
async function runChain() {
  const model = new OpenAI({ temperature: 0.9 });
  const prompt = PromptTemplate.fromTemplate(
    `The following is a friendly conversation between a human and an AI. The AI is talkative and provides lots of specific details from its context. If the AI does not know the answer to a question, it truthfully says it does not know.

  Relevant pieces of previous conversation:
  {history}

  (You do not need to use these pieces of information if not relevant)

  Current conversation:
  Human: {input}
  AI:`
  );

  const chain = new LLMChain({ llm: model, prompt, memory });

  

  const res1 = await chain.invoke({ input: "how about my first love?" });
  console.log({ res1 });

  const res2 = await chain.invoke({ input: "what's my name?" });
  console.log({ res2 });

  const res3 = await chain.invoke({ input: "what's kind of music i like?" });
  console.log({ res3 });
}

// Main function to execute the example
async function main() {
  await saveToMemory();
  await retrieveFromMemory("what places have I visited?");
  await runChain();
}

main().catch((error) => console.error("Error:", error));
