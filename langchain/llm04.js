const { createTaggingChain } = require("langchain/chains");
const { ChatOpenAI } = require("@langchain/openai");
require("dotenv").config();

const schema = {
  type: "object",
  properties: {
    sentiment: { type: "string" },
    tone: { type: "string" },
    language: { type: "string" },
  },
  required: ["tone"],
};

const chatModel = new ChatOpenAI({
  apiKey: process.env.OPENAI_API_KEY,
  model: "gpt-4-0613",
  temperature: 0,
});

const chain = createTaggingChain(schema, chatModel);

(async () => {
  console.log(
    await chain.run(
      `วันนี้สนุกมากเลย!`
    )
  );
})();

