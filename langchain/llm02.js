//NOTE Multiple Chains

const { PromptTemplate } = require("@langchain/core/prompts");
const { RunnableSequence } = require("@langchain/core/runnables");
const { StringOutputParser } = require("@langchain/core/output_parsers");
const { ChatOpenAI } = require("@langchain/openai");
require("dotenv").config();

const DEFAULT_MODEL_NAME = "gpt-3.5-turbo";

// Define the prompt templates
const prompt1 = PromptTemplate.fromTemplate(
  `What is the current weather in {city}?`
);
const prompt2 = PromptTemplate.fromTemplate(
  `Based on the weather {weather}, what activity would you recommend? please response in {lang}`
);

// Initialize the model
const model = new ChatOpenAI({
  apiKey: process.env.OPENAI_API_KEY,
  modelName: DEFAULT_MODEL_NAME,
});

// Chain to get the current weather
const weatherChain = prompt1.pipe(model).pipe(new StringOutputParser());

// Combined chain to recommend an activity based on the weather
const combinedChain = RunnableSequence.from([
  async (input) => {
    const weatherResult = await weatherChain.invoke(input);
    return { weather: weatherResult, lang: input.lang };
  },
  prompt2,
  model,
  new StringOutputParser(),
]);

// Invoke the combined chain
(async () => {
  const result = await combinedChain.invoke({
    city: "Tokyo",
    lang: "Thai"
  });

  console.log(result);
})();
