//NOTE Similarity Search

const { MemoryVectorStore } = require("langchain/vectorstores/memory");
const { OpenAIEmbeddings } = require("@langchain/openai");
require("dotenv").config();

async function main() {
  try {
    // Check if API key is provided
    if (!process.env.OPENAI_API_KEY) {
      throw new Error("OpenAI API key not found. Please set OPENAI_API_KEY environment variable.");
    }

    // Create MemoryVectorStore with texts and metadata
    const texts = [
      "Banana: elongated, sweet",
      "Apple: round, crisp",
      "Orange: round, tangy",
      "Grape: small, sweet",
      "Watermelon: large, juicy",
    ];
    const metadata = [
      { id: 1, name: "Banana" },
      { id: 2, name: "Apple" },
      { id: 3, name: "Orange" },
      { id: 4, name: "Grape" },
      { id: 5, name: "Watermelon" },
    ];
    const vectorStore = await MemoryVectorStore.fromTexts(
      texts,
      metadata,
      new OpenAIEmbeddings({
        apiKey: process.env.OPENAI_API_KEY,
        model: "text-embedding-ada-002",
      })
    );

    // Perform similarity search
    const query = "round, crisp";
    const numResults = 1;
    const results = await vectorStore.similaritySearch(query, numResults);

    // Output the results
    console.log(`Similarity search results for "${query}":`);
    results.forEach((result, index) => {
      console.log(`Result ${index + 1}:`);
      console.log(`- Fruit: ${result.metadata.name}`);
      console.log(`- Description: ${result.pageContent}`);
    });
  } catch (error) {
    console.error("Error:", error.message);
  }
}

main();
