
//NOTE Basic Prompt

const { ChatOpenAI } = require("@langchain/openai");
const { PromptTemplate } = require("@langchain/core/prompts");
require("dotenv").config();
const DEFAULT_MODEL_NAME="gpt-3.5-turbo"

const model = new ChatOpenAI({
  apiKey: process.env.OPENAI_API_KEY,
  modelName: DEFAULT_MODEL_NAME,
});

const promptTemplate = PromptTemplate.fromTemplate(
  "รู้จักการ์ตูนเรื่อง {topic} ไหม"
);

const chain = promptTemplate.pipe(model);

(async () => {
  const result = await chain.invoke({ topic: "pokemon" });
  console.log(result);
})();

